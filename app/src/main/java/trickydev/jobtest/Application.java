package trickydev.jobtest;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import trickydev.jobtest.core.base.HelperFactory;


public class Application extends MultiDexApplication {

    public static RequestQueue queue;

    @Override
    public void onCreate() {
        super.onCreate();
        HelperFactory.setHelper(getApplicationContext());
        queue = Volley.newRequestQueue(this);
    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }

}
