package trickydev.jobtest.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

import trickydev.jobtest.Application;
import trickydev.jobtest.R;
import trickydev.jobtest.core.ProfileStorage;
import trickydev.jobtest.core.network.UserRequest;
import trickydev.jobtest.core.network.UserResponse;
import trickydev.jobtest.ui.MainActivity;

public class RegisterFragment extends Fragment {

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.register_fragment, container, false);

        Button button = convertView.findViewById(R.id.email_sign_in_button);
        final EditText loginreg = convertView.findViewById(R.id.loginreg);
        final EditText password2 = convertView.findViewById(R.id.password2);
        final EditText password3 = convertView.findViewById(R.id.password3);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (password2.getText().length()<8 || password2.getText().length()>500 || password3.getText().length()<8 || password3.getText().length()>500 ) {
                    Toast.makeText(getContext(), R.string.password_error, Toast.LENGTH_SHORT).show();
                    return;
                }

                if(password2.getText().toString().equals(password3.getText().toString()))
                {
                    UserRequest userRequest = new UserRequest(loginreg.getText().toString(),
                            password2.getText().toString()
                    );
                    sendNetworkRequest(userRequest);
                } else
                    Toast.makeText(getContext(), "Пароли не совпадают", Toast.LENGTH_LONG).show();

            }
        });

        return convertView;
    }
    private void sendNetworkRequest(final UserRequest userRequest) {

        final String requestBody = new Gson().toJson(userRequest);
        StringRequest request = new StringRequest(Request.Method.POST,
                "http://213.184.248.43:9099/api/account/signup",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response==null)
                        {
                            Toast.makeText(getContext(), R.string.error_smth, Toast.LENGTH_SHORT)
                                    .show();
                            return;
                        }
                        UserResponse userResponse = new Gson().fromJson(response, UserResponse.class);
                        if (userResponse.getStatus()==400)
                        {
                            Toast.makeText(getContext(), R.string.already_use, Toast.LENGTH_SHORT)
                                    .show();
                            return;
                        }
                        if (!userResponse.getData().getLogin().equals(userRequest.getLogin()))
                        {
                            Toast.makeText(getContext(), R.string.error_server, Toast.LENGTH_SHORT)
                                    .show();
                            return;
                        }

                        ProfileStorage.getInstance(getContext()).commitToken(userResponse.getData().getToken());
                        ProfileStorage.getInstance(getContext()).commitId(userResponse.getData().getUserId());
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode == 400){
                    Toast.makeText(getContext(), R.string.already_use, Toast.LENGTH_SHORT)
                            .show();
                }else {
                    Toast.makeText(getContext(), R.string.error_smth, Toast.LENGTH_SHORT)
                            .show();
                }
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };
        Application.queue.add(request);
    }

}
