package trickydev.jobtest.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import trickydev.jobtest.Application;
import trickydev.jobtest.R;
import trickydev.jobtest.core.ProfileStorage;
import trickydev.jobtest.core.network.UserRequest;
import trickydev.jobtest.core.network.UserResponse;
import trickydev.jobtest.ui.MainActivity;

public class LoginFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.login_fragment, container, false);

        //gvMain = (GridView)convertView.findViewById(R.id.gvMain);
        Button button = convertView.findViewById(R.id.email_sign_in_button);
        final EditText emailE = convertView.findViewById(R.id.emailE);
        final EditText password = convertView.findViewById(R.id.password);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (password.getText().length()<8 || password.getText().length()>500 ) {
                    Toast.makeText(getContext(), R.string.password_error, Toast.LENGTH_SHORT).show();
                    return;
                }

                UserRequest userRequest = new UserRequest(emailE.getText().toString(),
                        password.getText().toString()
                );
                sendNetworkRequest(userRequest);
            }
        });
        return convertView;
    }

    private void sendNetworkRequest(final UserRequest userRequest) {

        final String requestBody = new Gson().toJson(userRequest);
        StringRequest request = new StringRequest(Request.Method.POST,
                "http://213.184.248.43:9099/api/account/signin",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response==null)
                        {
                            Toast.makeText(getContext(), R.string.error_smth, Toast.LENGTH_SHORT)
                                    .show();
                            return;
                        }
                        UserResponse userResponse = new Gson().fromJson(response, UserResponse.class);
                        if (!userResponse.getData().getLogin().equals(userRequest.getLogin()))
                        {
                            Toast.makeText(getContext(), R.string.error_server, Toast.LENGTH_SHORT)
                                    .show();
                            return;
                        }

                        ProfileStorage.getInstance(getContext()).commitToken(userResponse.getData().getToken());
                        ProfileStorage.getInstance(getContext()).commitId(userResponse.getData().getUserId());
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), R.string.error_smth, Toast.LENGTH_SHORT)
                        .show();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                     return null;
                }
            }
        };
        Application.queue.add(request);

    }


}
