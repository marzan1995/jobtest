package trickydev.jobtest.ui.adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import trickydev.jobtest.R;
import trickydev.jobtest.core.base.Item;
import trickydev.jobtest.core.data.DataController;


public class DetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private AppCompatActivity mContext;
    private Item mItem;

    public DetailsAdapter(AppCompatActivity mContext, Item mItem) {
        this.mContext = mContext;
        this.mItem = mItem;
        DataController.getInstance().getComments(mItem.getId(), new DataController.CommentsBlock() {
            @Override
            public void OnCommentsFetched() {

            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType==TYPE_HEADER)
        {
            return new DetailsHeaderHolder(LayoutInflater.from(mContext)
                    .inflate(R.layout.item_header_details, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DetailsHeaderHolder)
        {
            Glide.with(mContext)
                    .load(mItem.getUrl())
                    .apply(new RequestOptions().fitCenter())
                    .into((ImageView) holder.itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0)
        {
            return TYPE_HEADER;
        }
        else
            return TYPE_COMMENT;
    }

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_COMMENT = 1;

    public static class DetailsHeaderHolder extends RecyclerView.ViewHolder{

        public DetailsHeaderHolder(View itemView) {
            super(itemView);
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }
}
