package trickydev.jobtest.ui.adapter;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import trickydev.jobtest.R;
import trickydev.jobtest.core.base.Item;
import trickydev.jobtest.core.data.DataController;
import trickydev.jobtest.ui.DetailsActivity;

public class GridPhotoAdapter extends RecyclerView.Adapter {

    private AppCompatActivity mContext;

    public GridPhotoAdapter(AppCompatActivity mContext) {
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = new GridPhotoHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.item_grid_photo, parent, false));

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof GridPhotoHolder)
        {
            final Item item = DataController.getInstance().getData().get(position);
            Glide.with(mContext)
                    .load(item.getUrl())
                    .apply(new RequestOptions().centerCrop())
                    .into(((GridPhotoHolder) holder).imageView);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, DetailsActivity.class);
                    intent.putExtra(DetailsActivity.PARAM_ID, item.getId());
                    mContext.startActivity(intent);
                }
            });
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle(R.string.text_delete);
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            DataController.getInstance().removeItem(item, new DataController.DeleteBlock() {
                                @Override
                                public void OnRemoved(Item item) {
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, getItemCount());
                                }
                            });
                            dialogInterface.dismiss();
                        }
                    });
                    builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.show();
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return DataController.getInstance().size();
    }

    public static class GridPhotoHolder extends RecyclerView.ViewHolder{

        private ImageView imageView;

        public GridPhotoHolder(View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.targetImage);
        }
    }
}
