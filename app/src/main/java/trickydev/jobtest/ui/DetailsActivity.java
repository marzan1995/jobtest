package trickydev.jobtest.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.sql.SQLException;

import trickydev.jobtest.R;
import trickydev.jobtest.core.base.HelperFactory;
import trickydev.jobtest.core.base.Item;
import trickydev.jobtest.ui.adapter.DetailsAdapter;

public class DetailsActivity extends AppCompatActivity {

    public static final String PARAM_ID = "id";

    private Item mRetreived;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        if (getIntent()== null ||getIntent().getExtras()==null){
            finish();
            return;
        }

        try {
            mRetreived = HelperFactory.getHelper().getItemDAO().queryForId(getIntent().getIntExtra(PARAM_ID, 0));
        } catch (SQLException e) {
            finish();
            return;
        }
        RecyclerView recyclerView = findViewById(R.id.recView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setSupportsChangeAnimations(false);

        recyclerView.setItemAnimator(defaultItemAnimator);
        recyclerView.setAdapter(new DetailsAdapter(this, mRetreived));

    }
}
