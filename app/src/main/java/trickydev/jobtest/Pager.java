package trickydev.jobtest;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import trickydev.jobtest.ui.fragment.LoginFragment;
import trickydev.jobtest.ui.fragment.RegisterFragment;


public class Pager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    public Pager(FragmentManager fm) {
        super(fm);
    }



    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                LoginFragment loginFragment = new LoginFragment();
                return loginFragment;
            case 1:
                RegisterFragment registerFragment = new RegisterFragment();
                return registerFragment;
            case 2:

            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Login";
            case 1:
                return "Register";
        }
        return null;
    }
}