package trickydev.jobtest.core.base;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "goals")

public class Item {
    public final static String GOAL_NAME_FIELD_NAME = "name";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = GOAL_NAME_FIELD_NAME)
    private String url;

    @DatabaseField(dataType = DataType.FLOAT)
    private float lat;

    @DatabaseField(dataType = DataType.FLOAT)
    private float lng;

    @DatabaseField(dataType = DataType.LONG)
    private long date;

    public Item(){
    }

    public String getUrl() {
        return url;
    }

    public int getId() {
        return id;
    }
}
