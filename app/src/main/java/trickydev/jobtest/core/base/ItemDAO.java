package trickydev.jobtest.core.base;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

public class ItemDAO extends BaseDaoImpl<Item, Integer> {

    protected ItemDAO(ConnectionSource connectionSource,
                      Class<Item> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Item> getAllItems() throws SQLException{
        return this.queryForAll();
    }
}