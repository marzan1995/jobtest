package trickydev.jobtest.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class ProfileStorage {

    private static ProfileStorage instance;
    //TODO LAZY THREAD SAFE SINGLETON
    public static ProfileStorage getInstance(Context context) {
        ProfileStorage localInstance = instance;
        if (localInstance == null) {
            synchronized (ProfileStorage.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ProfileStorage(context);
                }
            }
        }
        return localInstance;
    }

    private final static String PREF_NAME = "user_bucket";

    private SharedPreferences mSharedPreferences;

    public boolean isLogged()
    {
        return mToken!=null && mUserId != -1;
    }

    private String mToken=null;
    private Integer mUserId = null;

    private final static String USER_TOKEN = "saved_token";
    private final static String USER_ID = "saved_id";

    @Nullable
    public String getUserToken()
    {
        return mToken;
    }

    public void commitToken(@NonNull String newToken)
    {
        mToken = newToken;
        if (mSharedPreferences!=null)
            mSharedPreferences.edit().putString(USER_TOKEN, mToken).apply();
    }

    public void commitId(@NonNull Integer integer)
    {
        mUserId = integer;
        if (mSharedPreferences!=null)
            mSharedPreferences.edit().putInt(USER_ID, integer).apply();
    }

    private ProfileStorage(Context context)
    {
        mSharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        mToken = mSharedPreferences.getString(USER_TOKEN, null);
        mUserId = mSharedPreferences.getInt(USER_ID, -1);
    }

}
