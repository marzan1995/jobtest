package trickydev.jobtest.core.network;


public class UserResponse {
    private Integer status;
    private UserDispatched data;

    public UserResponse() {
    }

    public UserDispatched getData() {
        return data;
    }

    public void setData(UserDispatched data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
