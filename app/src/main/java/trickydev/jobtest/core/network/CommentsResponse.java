package trickydev.jobtest.core.network;

import java.util.List;

/**
 * Created by Rodion Bartoshik on 10/6/17.
 */

public class CommentsResponse {
    private Integer status;
    private List data;

    public List getData() {
        return data;
    }

    public Integer getStatus() {
        return status;
    }
}
