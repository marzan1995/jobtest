package trickydev.jobtest.core.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UserRequest {
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("password")
    @Expose
    private String password;

    public UserRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
