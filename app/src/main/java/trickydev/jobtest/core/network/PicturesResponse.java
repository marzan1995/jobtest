package trickydev.jobtest.core.network;

import java.util.List;

import trickydev.jobtest.core.base.Item;



public class PicturesResponse {
    private Integer status;
    private List<Item> data;

    public Integer getStatus() {
        return status;
    }

    public List<Item> getData() {
        return data;
    }

    public void setData(List<Item> data) {
        this.data = data;
    }
}
