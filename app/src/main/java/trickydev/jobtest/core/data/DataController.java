package trickydev.jobtest.core.data;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import trickydev.jobtest.Application;
import trickydev.jobtest.R;
import trickydev.jobtest.core.ProfileStorage;
import trickydev.jobtest.core.base.HelperFactory;
import trickydev.jobtest.core.base.Item;
import trickydev.jobtest.core.network.CommentsResponse;
import trickydev.jobtest.core.network.PicturesResponse;
import trickydev.jobtest.ui.MainActivity;
import trickydev.jobtest.ui.MapsActivity;


public class DataController {

    private DataDelegate delegate;

    private static final String TAG = DataController.class.getName();
    private static DataController instance;
    public static DataController getInstance() {
        DataController localInstance = instance;
        if (localInstance == null) {
            synchronized (DataController.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new DataController();
                }
            }
        }
        return localInstance;
    }

    private List<Item> mData;

    public List<Item> getData()
    {
        return mData;
    }

    public DataController()
    {
        updateData();
    }

    private void updateData()
    {
        try {
            mData = HelperFactory.getHelper().getItemDAO().getAllItems();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        assert mData != null;
    }
    private Bitmap getPicture(Context context, String path) {
        // Get the dimensions of the View
        int targetW = 512;
        int targetH = 512;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
        return bitmap;
    }

    public void uploadImage(final Context context, String path, LatLng latLng)
    {
        //ENCODE BITMAP TO JPEG BASE64
        String encodedBitmap = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            getPicture(context, path).compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            encodedBitmap = Base64.encodeToString(byteArray, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        JSONObject jsonObject = new JSONObject();


        try {
            jsonObject.put("base64Image", encodedBitmap);
            jsonObject.put("date", 1);
            jsonObject.put("lat", latLng.latitude);
            jsonObject.put("lng", latLng.longitude);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String requestBody = jsonObject.toString();
        StringRequest request = new StringRequest(Request.Method.POST,
                        "http://213.184.248.43:9099/api/image",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("qwe", response);
                        Toast.makeText(context, " " + response,
                                Toast.LENGTH_SHORT).show();
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Toast.makeText(context, error.getMessage() + " " + new String(error.networkResponse.data),
                            Toast.LENGTH_SHORT).show();
                    Log.w(TAG, error.getMessage() + " " + new String(error.networkResponse.data));
                }
                catch (Exception e)
                {
                    Toast.makeText(context, context.getString(R.string.error_smth),
                            Toast.LENGTH_SHORT).show();
                }
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("Access-Token", ProfileStorage.getInstance(null).getUserToken());
                return stringMap;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };
        Application.queue.add(request);
    }

    public static final int PAGE_SIZE = 20;

    public void loadPage(int page)
    {
        if (size()<=page*PAGE_SIZE) {
            StringRequest request = new StringRequest(Request.Method.GET,
                    String.format(Locale.UK,
                            "http://213.184.248.43:9099/api/image?page=%d",
                            page),
                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (response!=null) {
                                List<Item> itemsList = new Gson().fromJson(response,PicturesResponse.class).getData();
                                Log.d("TAG", "success"+response);

                                for (Item item : itemsList) {
                                    try {
                                        if (!HelperFactory.getHelper().getItemDAO().idExists(item.getId())) {
                                            HelperFactory.getHelper().getItemDAO().create(item);
                                        }
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                    }
                                }
                                updateData();
                                Handler handler = new Handler(Looper.getMainLooper());
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (delegate!=null)
                                            delegate.OnDataChanged();
                                    }
                                });
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        Log.d("TAG", error.getMessage() + new String(error.networkResponse.data));
                    }
                    catch (Exception e)
                    {

                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> stringMap = new HashMap<>();
                    stringMap.put("Accept", "*/*");
                    stringMap.put("Access-Token", ProfileStorage.getInstance(null).getUserToken());
                    return stringMap;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
            };
            Application.queue.add(request);
        }
    }

    public void getComments(int imageId, final CommentsBlock withBlock)
    {
        StringRequest request = new StringRequest(Request.Method.GET,
                String.format(Locale.UK,
                        "http://213.184.248.43:9099/api/%d/comment?page=%d", imageId,0),
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response!=null) {
                            List itemsList = new Gson().fromJson(response, CommentsResponse.class).getData();
                            Log.d("TAG", "success");
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (withBlock!=null)
                                        withBlock.OnCommentsFetched();
                                }
                            });
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.d("TAG", error.getMessage() + new String(error.networkResponse.data));
                }
                catch (Exception e)
                {

                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("Accept", "*/*");
                stringMap.put("Access-Token", ProfileStorage.getInstance(null).getUserToken());
                return stringMap;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        Application.queue.add(request);
    }

    public void removeItem(final Item item, final DeleteBlock withBlock)
    {
        StringRequest request = new StringRequest(Request.Method.DELETE,
                String.format(Locale.UK,
                        "http://213.184.248.43:9099/api/image/%d",
                        item.getId()),
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response!=null) {
                            Log.d("TAG", "success");
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        if (!HelperFactory.getHelper().getItemDAO().idExists(item.getId())) {
                                            HelperFactory.getHelper().getItemDAO().delete(item);
                                        }
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                    }
                                    updateData();
                                    withBlock.OnRemoved(item);
                                }
                            });
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.d("TAG", error.getMessage() + new String(error.networkResponse.data));
                }
                catch (Exception e)
                {

                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("Access-Token", ProfileStorage.getInstance(null).getUserToken());
                return stringMap;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        Application.queue.add(request);
    }

    public interface DeleteBlock{
        void OnRemoved(Item item);
    }

    public interface CommentsBlock{
        void OnCommentsFetched();
    }

    public int size()
    {
        return mData.size();
    }

    public void setDelegate(DataDelegate delegate) {
        this.delegate = delegate;
    }
}
