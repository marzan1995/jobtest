package trickydev.jobtest.core.data;

/**
 * Created by Rodion Bartoshik on 10/6/17.
 */

public interface DataDelegate {
    void OnDataChanged();
}
